package com.terminalab.proteintrackertests;

import com.terminalab.proteintracker.TrackingService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Created by muhamm4d on 3/12/2017.
 *
 */
public class TrackingServiceTests {
    @Test
    public void NewTrackingServiceTotalIsZero() {
        TrackingService service = new TrackingService();
        Assertions.assertEquals(0, service.getTotal());
    }
}
